from django.contrib import admin

from List.models import Student, Attendant, Institution, Course, Teacher, Grade, Register, Course_note

# Register your models here.
admin.site.register(Student)
admin.site.register(Attendant)
admin.site.register(Institution)
admin.site.register(Course)
admin.site.register(Teacher)
admin.site.register(Grade)
admin.site.register(Register)
admin.site.register(Course_note)