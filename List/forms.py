from django import forms

from .models import Student, Attendant, Grade, Register, Course_note, Course, Institution, Teacher


class StudentForm(forms.ModelForm):
    id = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    adress = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    age = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    date_of_birth = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control'}))
    attendants_name = forms.ModelChoiceField(queryset=Attendant.objects.all())

    attendants_name.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Student
        fields = ['id', 'name', 'last_name', 'adress', 'email', 'age', 'date_of_birth', 'attendants_name']

class AttendantForm(forms.ModelForm):
    id = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    age = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    profession = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Attendant
        fields = ['id', 'name', 'last_name', 'email', 'age','profession']


class InstitutionForm(forms.ModelForm):
    school_id = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    adress = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    phone = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Institution
        fields = ['school_id', 'name', 'adress', 'email', 'phone']


class CourseForm(forms.ModelForm):
    course_id = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    grade_id = forms.ModelChoiceField(queryset=Grade.objects.all())
    grade_id.widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Course
        fields = ['course_id', 'name', 'grade_id']


class TeacherForm(forms.ModelForm):
    teacher_id = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
   
    course = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Teacher
        fields = ['teacher_id', 'name', 'course', 'email']

class GradeForm(forms.ModelForm):
    name_grade = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Grade
        fields = ['name_grade']


class RegisterForm(forms.ModelForm):
    student_id = forms.ModelChoiceField(queryset=Student.objects.all())
    grade_id = forms.ModelChoiceField(queryset=Grade.objects.all())

    class Meta:
        model = Register
        fields = ['student_id', 'grade_id']

class NoteForm(forms.ModelForm):
    course_id = forms.ModelChoiceField(queryset=Course.objects.all())
    student_id = forms.ModelChoiceField(queryset=Student.objects.all())
    period = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    note = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    course_id.widget.attrs.update({'class': 'form-control'})
    student_id.widget.attrs.update({'class': 'form-control'})
    class Meta:
        model = Course_note
        fields =['course_id', 'student_id', 'period', 'note']