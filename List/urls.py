from django.urls import path
from django.contrib.auth.decorators import login_required
from List.views import AddInstitution,AddCourse,AddTeacher, EditCourse,Inicio, EditStudent, AddAttendat, Attendantview,Courseview,EditTeacher, EditAttendant
from List.views import Indexview, AddStudent, lisGrade, AddGrade, EditGrade, listRegister, AddRegister, listNote, AddNote, EditNote, EditRe,Teacherview

urlpatterns = [
    path('', login_required(Inicio.as_view()), name='inicio'),
    path('student', login_required(Indexview.as_view()), name='student'),
    path('teacher', login_required(Teacherview.as_view()), name='teacher'),
    path('course', login_required(Courseview.as_view()), name='course'),
    path('attendant', login_required(Attendantview.as_view()), name='attendant'),
    path('student_form', login_required(AddStudent.as_view()), name='student_form'),
    path('attendand_form', login_required(AddAttendat.as_view()), name='attendart_form'),
    path('Edit_Student/<int:pk>', login_required(EditStudent.as_view()), name='EditStudent'),
    path('institution_form', login_required(AddInstitution.as_view()), name='institution_form'),
    path('Edit_Attendant/<int:pk>', EditAttendant.as_view(), name='EditAttendant'),
    path('course_form', login_required(AddCourse.as_view()), name='course_form'),
    path('teacher_form', login_required(AddTeacher.as_view()), name='teacher_form'),
    path('lisgrade', login_required(lisGrade.as_view()),name='grades'),
    path('insert_grade', login_required(AddGrade.as_view()), name='insert_grade'),
    path('edit_grade/<int:pk>', login_required(EditGrade.as_view()), name='edit_grade'),
    path('lisregister', login_required(listRegister.as_view()), name='lisregister'),
    path('insert_register', login_required(AddRegister.as_view()), name='insert_register'),
    path('edit_register/<int:pk>', login_required(EditRe.as_view()), name='edit_register'),
    path('lis_note', login_required(listNote.as_view()), name='lis_note'),
    path('insert_note', login_required(AddNote.as_view()), name='insert_note'),
    path('edit_note/<int:pk>', login_required(EditNote.as_view()), name='edit_note'),
    path('Edit_Course/<int:pk>', login_required(EditCourse.as_view()), name='EditCourse'),
    path('Edit_Teacher/<int:pk>', login_required(EditTeacher.as_view()), name='EditTeacher')



]

