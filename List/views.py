from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, CreateView, UpdateView

from List.models import Student, Grade, Register, Course_note, Attendant,Institution, Course,Teacher
from List.forms import GradeForm, RegisterForm, NoteForm

from List.forms import StudentForm, AttendantForm
from List.forms import InstitutionForm
from List.forms import CourseForm
from List.forms import TeacherForm

class Inicio(ListView):
    model = Student
    template_name = 'inicio.html'

class Indexview(ListView):
    model = Student
    queryset = Student.objects.all()
    template_name = 'student.html'
    context_object_name = 'students'

class Attendantview(ListView):
    model = Attendant
    queryset = Attendant.objects.all()
    template_name = 'attendant.html'
    context_object_name = 'attendant'

class AddStudent(CreateView):
    model = Student
    form_class = StudentForm
    template_name = 'student_form.html'
    success_url = 'student'
    context_object_name = 'editstudent'

class Teacherview(ListView):
    model = Teacher
    queryset = Teacher.objects.all()
    template_name = 'teacher.html'
    context_object_name = 'teacher'

class Courseview(ListView):
    model = Course
    queryset = Course.objects.all()
    template_name = 'course.html'
    context_object_name = 'course'

class AddTeacher(CreateView):
    model = Teacher
    form_class = TeacherForm
    template_name = 'teacher_form.html'
    success_url = 'teacher'



class AddAttendat(CreateView):
    model = Attendant
    form_class = AttendantForm
    template_name = 'attendant_form.html'
    success_url = 'attendant'
    context_object_name = 'editAttendant'

class EditAttendant(UpdateView):
    model = Attendant
    form_class = AttendantForm
    pk_url_kwarg = 'pk'
    template_name = 'Edit_attendant.html'
    success_url = '/attendant'

class EditStudent(UpdateView):
    model = Student
    form_class = StudentForm
    pk_url_kwarg = 'pk'
    template_name = 'Edit_student.html'
    success_url = '/student'

class AddInstitution(CreateView):
    model = Institution
    form_class = InstitutionForm
    template_name = 'institution_form.html'
    success_url = '/'

class AddCourse(CreateView):
    model = Course
    form_class = CourseForm
    template_name = 'course_form.html'
    success_url = '/'




class lisGrade(ListView):
    model = Grade
    queryset = Grade.objects.all()
    template_name = 'lisgrade.html'
    context_object_name = 'grades'


class AddGrade(CreateView):
    model = Grade
    form_class = GradeForm
    template_name = 'insert_grade.html'
    success_url = 'lisgrade'


class EditGrade(UpdateView):
    model = Grade
    form_class = GradeForm
    pk_url_kwarg = 'pk'
    template_name = 'edit_grade.html'
    success_url = '/'


class listRegister(ListView):
    model = Register
    queryset = Register.objects.all()
    template_name = 'lisregister.html'
    context_object_name = 'register'

class AddRegister(CreateView):
    model = Register
    form_class = RegisterForm
    template_name = 'insert_register.html'
    success_url = 'lisregister'

class listNote(ListView):
    model = Course_note
    queryset = Course_note.objects.all()
    template_name = 'lis_note.html'
    context_object_name = 'notes'

class EditNote(UpdateView):
    model = Course_note
    form_class = NoteForm
    pk_url_kwarg = 'pk'
    template_name = 'edit_note.html'
    success_url = 'lis_note'


class AddNote(CreateView):
    model = Course_note
    form_class = NoteForm
    template_name = 'insert_note.html'
    success_url = 'lis_note'

class EditRe(UpdateView):
    model = Register
    form_class = RegisterForm
    pk_url_kwarg = 'pk'
    template_name = 'edit_grade.html'
    success_url = 'lisregister'

class EditCourse(UpdateView):
    model = Course
    form_class = CourseForm
    pk_url_kwarg = 'pk'
    template_name = 'Edit_course.html'
    success_url = '/'

class EditTeacher(UpdateView):
    model = Teacher
    form_class = TeacherForm
    pk_url_kwarg = 'pk'
    template_name = 'Edit_teacher.html'
    success_url = '/'