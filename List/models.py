from django.db import models


# Create your models here.

class Attendant(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    age = models.IntegerField()
    profession = models.CharField(max_length=50)

    def __str__(self):
        return (self.name + ' ' + self.last_name)


class Student(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    adress = models.CharField(max_length=100)
    email = models.EmailField()
    age = models.IntegerField()
    date_of_birth = models.DateField()
    attendants_name = models.ForeignKey('Attendant', on_delete=models.CASCADE)

    def __str__(self):
        return (self.name + ' ' + self.last_name)




class Institution(models.Model):
    school_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    adress = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.IntegerField()

class Grade(models.Model):
    grade_id = models.AutoField(primary_key=True)
    name_grade = models.CharField(max_length=50)
    def __str__(self):
        return (self.name_grade)



class Teacher(models.Model):
    teacher_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    
    email = models.EmailField()

    def __str__(self):
        return (self.name)

class Course(models.Model):
    course_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    grade_id = models.ForeignKey(Grade, on_delete=models.CASCADE)

    def __str__(self):
        return (self.name)


class Register(models.Model):
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    grade_id = models.ForeignKey(Grade, on_delete=models.CASCADE)
    date = models.DateField(auto_now=True)


class Course_note(models.Model):
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    period = models.IntegerField()
    year = models.DateField(auto_now_add=True)
    note = models.FloatField()


